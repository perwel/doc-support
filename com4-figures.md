![Alt text](http://g.gravizo.com/source/gravizosample2?https%3A%2F%2Fbitbucket.org%2Fperwel%2Fdoc-support%2Fraw%2Fmaster%2Fcom4-figures.md#
flow
  digraph G {
   "Radius Server" -> "Record publisher" [ label = " Store json file"];
   "Record publisher" -> "Record subscriber"  [ label = " Publish to Google Pub/Sub"];
   "Record subscriber" -> "Google Heat Map"  [ label = " Store in Google Big Query"];
 }
flow
)